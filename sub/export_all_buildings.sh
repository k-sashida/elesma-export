#/bin/bash
####################################################################################################
#
# ファイル名:csvエクスポートサブシェル
# 物理名    :export_all_buildings.sh
# 機能概要  : csvexport.shより呼び出されるサブシェル。
#             本サブシェルを直接実行することも可能。(下記、実行形式例を参照)
#             実行時には-a|a（全件) -e|e(有効なビルのみ）を選択して実行する。
#             環境変数定義ファイルと共通関数ファイルの、存在チェック、および読み込みを行ったのち、
#             ビルマスタ、ビル多言語マスタ、エレベータマスタ、エレベータ多言語マスタ、
#             および、フロアマスタを全件csv形式で出力する
# 実行形式例: ./export_all_buildings.sh -a
# 入力:
#   引数:
#      $1 有効フラグ(-a または -e) (必須)
#         -a 全件出力
#         -e 有効なビルのみ出力
#   ファイル:
#      csvexport.properties  :環境変数定義ファイル
#      common.func           :共通関数定義ファイル
# 出力:
#    戻り値:メッセージ
#       0(正常) : なし
#       9(異常) : 環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}
#       9(異常) : 共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}
#       9(異常) : なし(function_inputfile_check関数で出力されたメッセージを表示)
#       9(異常) : なし(function_csv_export関数で出力されたメッセージを表示)
#    ファイル:説明 
#       ${OUTPUTDIR}/${_target}.csv                :各csvファイル
#       ${SUCCESSFILE}                             :正常終了時のみ出力する空ファイル
#       ${ERRORLOG}                                :エラーログ(異常終了時のみ)
#
####################################################################################################
####################################################################################################
#  初期処理
####################################################################################################
#環境変数設定
ROOTDIR=$(cd $(dirname $0); cd ../ ;pwd)
PROPERTY_FILE=${ROOTDIR}/conf/csvexport.properties
FUNCTION_FILE=${ROOTDIR}/conf/common.func
OUTPUTFILEPREFIX=all_buildings
## ファイルチェック
#環境変数定義ファイルチェック
if [ ! -s ${PROPERTY_FILE} ];then
  echo "環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}"
  exit 9
fi

#共通関数定義定義ファイルチェック
if [ ! -s ${FUNCTION_FILE} ];then
  echo "共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}"
  exit 9
fi
#環境変数、共通関数の読み込み
source ${PROPERTY_FILE}
source ${FUNCTION_FILE}

#出力ディレクトリ作成
mkdir -p ${OUTPUTDIR}

####################################################################################################
# メイン関数
####################################################################################################
function_main(){
  while :
  do
    #画面表示処理(csvexport.shから実行された場合のみ)
    if [ ${#} -eq 0 ];then
      #画面表示処理(csvexport.shから実行された場合のみ)
      read -p "全件出力[a]、または、有効なビルのみ出力[e]を選択してください。[a or e or q]" ANS
    else
      #サブシェルを直接実行した場合はユーザ応答はさせない
      ANS=$1
    fi
    case "${ANS}" in
      'a'|'-a'|'e'|'-e')
        #CSVのエクスポート 
        #全件[a]か有効[e]が確定した後に、環境変数を設定する
        ERRORLOG=${OUTPUTDIR}/${OUTPUTFILEPREFIX}_${ANS/-/}_error.log
        SUCCESSFILE=${OUTPUTDIR}/${OUTPUTFILEPREFIX}_${ANS/-/}_success
        for _target in ${BUILDINGS_TABLENAME_ARRAY[@]} 
        do
          #全件取得のときは、第3引数（建物番号)を''文字にして関数をコール
          function_csv_export export_${_target}.sql ${_target}.csv '' ${ANS} -a
          rtn=`echo $?`
          if [[ ${rtn} -eq 0 ]];then
            
            #ローカル変数定義
            _template_file=${TEMPLATEDIR}/template_${_target}.csv
            _output_csvworkfile=${WORKDIR}/${_target}.csv
            #csvファイルの末尾のカラムが空白(null)の場合、psqlコマンドだとカンマが付与されずimportに失敗するため
            #強制的に末尾にカンマを付与
            # 2019.08.16 m_elevators.slowlyClosedDoorSupportFlgにNOT NULL制約を付与しているためコメントアウト
            #sed -i -e s/$/,/g ${_output_csvworkfile}
            
            _csvfile=${OUTPUTDIR}/${_target}.csv            
            
            cat ${_template_file} ${_output_csvworkfile} > ${_csvfile} 2>${ERRORLOG}
            echo "csvファイルを作成しました。${_csvfile}"
            rm -f ${WORKDIR}/${_target}.csv  >> ${ERRORLOG} 2>&1
          else
            exit 9
          fi
        done
        touch ${SUCCESSFILE}
        #エラーログが空だった場合は削除処理を行う
        find ${OUTPUTDIR}/*error* -empty | xargs rm  > /dev/null 2>&1
        break
        ;;

      "q")
        echo "q"
        exit 1
        ;;
      "*") 
        echo "キーが違います"
    esac
  done
}
#####################################################################################################
#メイン処理
#####################################################################################################
#ファイルチェック関数
function_inputfile_check
rtn=`echo $?`
if [ ${rtn} -ne 0 ];then
  exit 9
fi

if [ ${#} -eq 0 ];then
  #親シェルから実行した場合
  function_main
 
else
  #サブシェルを直接実行した場合
  if [ ${#} -ne 1 ] || [ ${1} != "-a" ] && [ ${1} != "-e" ];then
    echo "引数を正しく指定してください"
    echo "  全件出力の場合: export_all_buildings.sh -a"
    echo "  有効なビルのみ出力する場合: export_all_buildings.sh -e"
    exit 1
  fi

  function_main $1

fi
