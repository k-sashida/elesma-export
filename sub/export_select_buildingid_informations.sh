#/bin/bash
####################################################################################################
#
# ファイル名:おしらせcsvエクスポート(建物番号指定)サブシェル
# 物理名    : export_select_buildingid_informations.sh
# 機能概要  : csvexport.shより呼び出されるサブシェル。
#             本サブシェルを直接実行することも可能。(下記、実行形式例を参照)
#             ユーザが入力した建物番号を検索キーにお知らせマスタ、お知らせ多言語マスタを
#             csv形式で出力する
# 実行形式例: ./export_select_buildingid_informations.sh 9999999
#
# 入力:
#   引数:
#      $1 建物番号（半角数字7桁:任意）
#   ファイル:
#      csvexport.properties  :環境変数定義ファイル
#      common.func           :共通関数定義ファイル
# 出力:
#    戻り値:メッセージ
#       0(正常) : なし
#       9(異常) : 環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}
#       9(異常) : 共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}
#       9(異常) : なし(function_inputfile_check関数で出力されたメッセージを表示)
#       9(異常) : なし(function_csv_export関数で出力されたメッセージを表示)
#    ファイル:説明 
#       ${OUTPUTDIR}/${_target}.csv                :各csvファイル
#       ${SUCCESSFILE}                             :正常終了時のみ出力する空ファイル
#       ${ERRORLOG}                                :エラーログ(異常終了時のみ)
#
####################################################################################################
####################################################################################################
#  初期処理
####################################################################################################
#環境変数設定
ROOTDIR=$(cd $(dirname $0); cd ../ ;pwd)
PROPERTY_FILE=${ROOTDIR}/conf/csvexport.properties
FUNCTION_FILE=${ROOTDIR}/conf/common.func
OUTPUTFILEPREFIX=select_buildingid_informations
## ファイルチェック
#環境変数定義ファイルチェック
if [ ! -s ${PROPERTY_FILE} ];then
  echo "環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}"
  exit 9
fi

#共通関数定義定義ファイルチェック
if [ ! -s ${FUNCTION_FILE} ];then
  echo "共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}"
  exit 9
fi
#環境変数、共通関数の読み込み
source ${PROPERTY_FILE}
source ${FUNCTION_FILE}

#出力ディレクトリ作成
mkdir -p ${OUTPUTDIR}

#CSVエクスポート実行
function_main(){
  while :
  do
    #画面表示処理
    eval ${BASHEXEC} ${SUBSHELLDIR}/export_show_building.sh $1
    rtn=`echo $?`
    if [ ${rtn} -ne 0 ];then
      echo "処理を終了します"
      return ${rtn}
    fi
    read -p "こちらでよろしければ y を押してください。[y or q]" ANS
    case "${ANS}" in
      "y")
        #CSVのエクスポート
        #建物番号取得後に環境変数を設定
        BUILDING_ID=`grep -v '^$' ${WORKDIR}/${SQLWORKFILE}  | awk -F'|' '{print $1}' | uniq | sed -e 's/\s//g'`
        ERRORLOG=${OUTPUTDIR}/${OUTPUTFILEPREFIX}_${BUILDING_ID}_error.log
        SUCCESSFILE=${OUTPUTDIR}/${OUTPUTFILEPREFIX}_${BUILDING_ID}_success
        for _target in ${INFORMATION_TABLENAME_ARRAY[@]}
        do
          function_csv_export export_${_target}.sql ${_target}.csv ${BUILDING_ID}
          rtn=`echo $?`
          if [[ ${rtn} -eq 0 ]];then
            #ローカル変数定義
            _template_file=${TEMPLATEDIR}/template_${_target}.csv
            _output_csvworkfile=${WORKDIR}/${_target}.csv
            _csvfile=${OUTPUTDIR}/${_target}.csv
            #csvファイルの末尾のカラムが空白の場合、psqlコマンドだとカンマが付与されずimportに失敗するため
            #強制的に末尾にカンマを付与
            # 2019.08.16 m_elevators.slowlyClosedDoorSupportFlgにNOT NULL制約を付与しているためコメントアウト
            #sed -i -e s/$/,/g ${_output_csvworkfile}
            
            cat ${_template_file} ${_output_csvworkfile} > ${_csvfile} 2>${ERRORLOG}
            echo "csvファイルを作成しました。${_csvfile}"

            rm -f ${WORKDIR}/${_target}.csv  >> ${ERRORLOG} 2>&1
          else
            exit 9
          fi
        done
        touch ${SUCCESSFILE}
        #エラーログが空だった場合は削除処理を行う
        find ${OUTPUTDIR}/*error* -empty | xargs rm  > /dev/null 2>&1
        break
        ;;

      "q")
        echo "q"
        # 2019.08.16 参照のみで処理中断されたため、処理終了後ディレクトリごと削除
        find ${OUTPUTDIR} | xargs rm -fr
        break
        ;;
      "*") 
        echo "キーが違います"
    esac
  done
}
#####################################################################################################
#メイン処理
#####################################################################################################
#ファイルチェック関数
function_inputfile_check
rtn=`echo $?`
if [ ${rtn} -ne 0 ];then
  exit 9
fi

if [ $# -eq 0 ];then
  #親シェルから実行した場合
  function_main
else
  #サブシェルを直接実行した場合
  function_main $1
fi


