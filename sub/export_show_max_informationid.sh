#/bin/bash
####################################################################################################
#
# ファイル名: おしらせID照会
# 物理名    : export_show_max_informationid.sh
# 機能概要  : csvexport.shから実行されるサブシェル
#             お知らせマスタからおしらせIDの最大値を抽出し、標準出力に出力、
#             およびファイル出力する。
#
# 実行形式例: ./export_show_max_informationid.sh
# 入力:
#   引数:
#      なし
#   ファイル:
#      csvexport.properties  :環境変数定義ファイル
#      common.func           :共通関数定義ファイル
# 出力:
#    戻り値:メッセージ
#       0(正常) : お知らせID最大値: ${_informationid}
#       9(異常) : 環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}
#       9(異常) : 共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}
#       9(異常) : なし(function_inputfile_check関数で出力されたメッセージを表示)
#    ファイル:説明 
#       ${OUTPUTDIR}/${OUTPUTPREFIX}.csv           :おしらせIDの最大値を書き込んだファイル
#       ${SUCCESSFILE}                             :正常終了時のみ出力する空ファイル
#       ${ERRORLOG}                                :エラーログ(異常終了時のみ)
#
####################################################################################################
####################################################################################################
#  初期処理
####################################################################################################
#環境変数設定
ROOTDIR=$(cd $(dirname $0); cd ../ ;pwd)
PROPERTY_FILE=${ROOTDIR}/conf/csvexport.properties
FUNCTION_FILE=${ROOTDIR}/conf/common.func
OUTPUTPREFIX=show_max_informationid
## ファイルチェック
#環境変数定義ファイルチェック
if [ ! -s ${PROPERTY_FILE} ];then
  echo "環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}"
  exit 9
fi

#共通関数定義定義ファイルチェック
if [ ! -s ${FUNCTION_FILE} ];then
  echo "共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}"
  exit 9
fi
#環境変数、共通関数の読み込み
source ${PROPERTY_FILE}
source ${FUNCTION_FILE}

##出力ディレクトリ作成
mkdir -p ${OUTPUTDIR}
ERRORLOG=${OUTPUTDIR}/${OUTPUTPREFIX}_error.log
SUCCESSFILE=${OUTPUTDIR}/${OUTPUTPREFIX}_success

#####################################################################################################
#メイン処理
#####################################################################################################
#ファイルチェック関数
function_inputfile_check
rtn=`echo $?`
if [ ${rtn} -ne 0 ];then
  exit 9
fi

#変数設定
ERRORLOG=${OUTPUTDIR}/${OUTPUTPREFIX}_error.log
SUCCESSFILE=${OUTPUTDIR}/${OUTPUTPREFIX}_success

#おしらせID出力用SQLの実行
##psqlコマンド実行時、パスワード入力を省略するための変数設定
export PGPASSWORD=${DBUSERPASSWORD}
${PSQLCMDPATH}/psql -U ${DBUSER} -d ${DBNAME} -p ${DBPORT} \
  -f ${SQLDIR}/export_show_max_informationid.sql \
  -v SCHEME=${SCHEMENAME} -t 1> ${OUTPUTDIR}/${OUTPUTPREFIX}.csv 2> ${ERRORLOG}
rtn=`echo $?`
if [ ${rtn} -ne 0 ];then
  echo "DB接続で異常が発生しました。エラーログを確認してください。"
  echo "エラーログ:${ERRORLOG}"
  echo ""
  exit 9
fi


#おしらせidを抽出し画面表示
_informationid=`cat  ${OUTPUTDIR}/${OUTPUTPREFIX}.csv`

echo "お知らせID最大値: ${_informationid}"

#終了処理
#touch ${SUCCESSFILE}
#エラーログが空だった場合は削除処理を行う
#find ${OUTPUTDIR}/*error* -empty | xargs rm  > /dev/null 2>&1
# 2019.08.16 参照のみなので処理終了後ディレクトリごと削除
find ${OUTPUTDIR} | xargs rm -fr

exit 0
