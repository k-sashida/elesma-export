#/bin/bash
####################################################################################################
#
# ファイル名: ビル情報照会
# 物理名    : export_show_building.sh
# 機能概要  : ユーザが入力した建物番号の、ビル情報、エレベータ情報、フロア情報を
#             標準出力する。
#             フロア情報についてはビーコンMajorが設定されている場合のみ出力する
#
# 実行形式例: ./export_show_building.sh 9999999
# 入力:
#   引数:
#      $1 建物番号（半角数字7桁:任意）
#   ファイル:
#      csvexport.properties  :環境変数定義ファイル
#      common.func           :共通関数定義ファイル
#      ${WORKDIR}/${SQLWORKFILE} : function_buildingid_select で作成されるSQLワークファイル
# 出力:
#    戻り値:メッセージ
#       0(正常) : なし
#       9(異常) : 環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}
#       9(異常) : 共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}
#       9(異常) : なし(function_inputfile_check関数で出力されたメッセージを表示)
#       9(異常) : SQLワークファイルが存在しません。${WORKDIR}/${SQLWORKFILE}
#    ファイル:説明 
#       ${ERRORLOG}                                :エラーログ(異常終了時のみ)
#
####################################################################################################
####################################################################################################
#  初期処理
####################################################################################################
#環境変数設定
ROOTDIR=$(cd $(dirname $0); cd ../ ;pwd)
PROPERTY_FILE=${ROOTDIR}/conf/csvexport.properties
FUNCTION_FILE=${ROOTDIR}/conf/common.func
OUTPUTPREFIX=show_building
## ファイルチェック
#環境変数定義ファイルチェック
if [ ! -s ${PROPERTY_FILE} ];then
  echo "環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}"
  exit 9
fi

#共通関数定義定義ファイルチェック
if [ ! -s ${FUNCTION_FILE} ];then
  echo "共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}"
  exit 9
fi
#環境変数、共通関数の読み込み
source ${PROPERTY_FILE}
source ${FUNCTION_FILE}

#出力ディレクトリ作成
mkdir -p ${OUTPUTDIR}

ERRORLOG=${OUTPUTDIR}/${OUTPUTPREFIX}_error.log

####################################################################################################
# メイン関数
####################################################################################################
function_main(){
  if [ ! -f ${WORKDIR}/${SQLWORKFILE} ];then
    echo "ワークファイルが存在しません。${WORKDIR}/${SQLWORKFILE}"
    exit 9
  fi
  
  #画面表示処理
  ##ビル情報
  _buildingid=`grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F "|" '{print $1}'| uniq`
  _buildingname=`grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F "|" '{print $2}'| uniq`
  _valid_flg=`grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F "|" '{print $3}'| uniq`
  _valid_date=`grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F "|" '{print $4}'| uniq`
  _invalid_date=`grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F "|" '{print $5}'| uniq`
  printf "[建物番号] - [ビル名] - [有効/無効] - [利用開始日] - [有効期限日]\n"
  printf "%7s - %2s - %s - %10s - %10s \n" ${_buildingid} "${_buildingname}" \
         ${_valid_flg} ${_valid_date} ${_invalid_date}
  echo ""
  
  _before_elevator_number=""
  
  while read line
  do
    ##エレベータ情報
    _elevator_number=`echo ${line} | awk -F " " '{print $1}'`
    _elevator_name=`echo ${line} | awk -F " " '{print $2}'`
    printf "[エレベータ番号] - [エレベータ名]"
    if [ "${_before_elevator_number}" != "${_elevator_number}" ];then
      #エレベータ番号毎に情報を出力する
      printf "\n%s - %s\n" ${_elevator_number} ${_elevator_name}
    fi
    
    #エレベータ単位にフロアのヘッダ情報を出力する
    if [ "${_before_elevator_number}" != "${_elevator_number}" ];then
      printf "[フロア名] - [ビーコンmajor] - [ビーコンminor]\n"
    fi    
     
    while read floorline
    do
      #フロア情報
      #ビーコンmajorがnullでないレコードが1つでもある場合に表示処理を行う
      _floor_name=`echo ${floorline} | awk -F"|" '{print $10}'`
      _beacon_major=`echo ${floorline} | awk -F"|" '{print $11}'`
      _beacon_minor=`echo ${floorline} | awk -F"|" '{print $12}'`
      printf "%5s - %3s - %3s\n" ${_floor_name} "${_beacon_major}" "${_beacon_minor}"
    done < <(grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F"|" '$6=='${_elevator_number}' && $9~/[0-9]/{print $0}')

    echo ""
  done < <(grep -v '^$' ${WORKDIR}/${SQLWORKFILE} | awk -F'|' '$6!=""{print $6 $7}' | uniq)
}

#####################################################################################################
#メイン処理
#####################################################################################################
#ファイルチェック関数
function_inputfile_check
rtn=`echo $?`
if [ ${rtn} -ne 0 ];then
  exit 9
fi

if [ $# -eq 0 ];then
  function_buildingid_select
  function_main
else
  function_building_id_formatcheck $1
  function_buildingid_select $1
  function_main $1
fi
