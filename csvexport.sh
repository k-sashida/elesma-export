#!/bin/bash
####################################################################################################
#
# ファイル名:csvエクスポートシェル
# 物理名    :csvexport.sh
# 機能概要  :
#            ①本処理で利用する定義ファイルの確認
#            ②メニューリストファイルに定義されたメニュー番号、メニュー名を表示する
#            ③ユーザのメニュー番号に応じた、サブシェルを呼び出す
# 実行形式例 : ./csvexport.sh (ルートディレクトリにて実行すること)
# 入力:
#   引数:
#       なし
#   ファイル:
#       csvexport.properties  :環境変数定義ファイル
#       common.func           :共通関数定義ファイル
#       menu.list             :メニューリストファイル
# 出力:
#    戻り値:メッセージ
#       0(正常) : 
#       9(異常) : 環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}
#       9(異常): 共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}
#       9(異常): メニューリストファイルが存在しません。: ${MENULIST_FILE}
#       9(異常): SQLファイルが存在しません: ${SQLDIR}/${_sql_file_target}
#       9(異常): サブシェルファイルが存在しません: ${SUBSHELLDIR}/${_subshell_file_target}
#       9(異常): テンプレートファイルが存在しません: ${TEMPLATEDIR}/${_template_file_target}
#       9(異常): 処理が異常終了しました。
#    ファイル:
#          なし
#
####################################################################################################
####################################################################################################
#  初期処理
####################################################################################################
#変数設定
ROOTDIR=$(cd $(dirname $0); pwd)
PROPERTY_FILE=${ROOTDIR}/conf/csvexport.properties
FUNCTION_FILE=${ROOTDIR}/conf/common.func
MENULIST_FILE=${ROOTDIR}/conf/menu.list

## ファイルチェック
#環境変数定義ファイルチェック
if [ ! -s ${PROPERTY_FILE} ];then
  echo "環境変数定義ファイルが存在しません。: ${PROPERTY_FILE}"
  exit 9
fi

#共通関数定義定義ファイルチェック
if [ ! -s ${FUNCTION_FILE} ];then
  echo "共通関数定義ファイルが存在しません。: ${FUNCTION_FILE}"
  exit 9
fi

#メニューリストファイルチェック
if [ ! -s ${MENULIST_FILE} ];then
  echo "メニューリストファイルが存在しません。: ${MENULIST_FILE}"
  exit 9
fi

#環境変数、共通関数の読み込み
source ${PROPERTY_FILE}
source ${FUNCTION_FILE}

####################################################################################################
#  メイン画面表示関数
####################################################################################################
function_menu()
{
  while :
  do
    #メニュー表示処理
    echo ""
    echo "【CSVエクスポートメニュー】"        
    echo ""
    echo "[ No ]-[項目]"
    #メニューリストファイルから1行ずつ読み込みメニューを表示
    while read line
    do
      _menu_num=`echo ${line} | awk -F "|" '{print $1}'`
      _menu_message=`echo ${line} | awk -F "|" '{print $2}'`
      echo "[ ${_menu_num} ] ${_menu_message}"
    done < <(grep -v "#" ${MENULIST_FILE})
    echo "[ q ] シェルを終了"
    echo ""
    read -p "項目を選択してください。Noで指定してください。[1-${_menu_num} or q]" ANS

    case "${ANS}" in
      #メニュー番号は1桁までしか許容しない判定文となっている
      #メニュー番号が2桁以上に増える場合は判定条件を見直すこと
      [1-${_menu_num}])
      
        #メニューリストファイルを選択されたNoで検索し、Noに紐づくサブシェルを実行
        _subshell_name=`awk  -F"|" '$1=='${ANS}'{print $3}' ${MENULIST_FILE}` 
        _menu_name=`awk  -F"|" '$1=='${ANS}'{print $2}' ${MENULIST_FILE}`

        #画面表示処理
        echo "【CSVエクスポートメニュー】"
        echo " [ ${ANS} ] ${_menu_name}"
        
        #サブシェルを実行する。
        eval ${BASHEXEC} ${SUBSHELLDIR}/${_subshell_name}
        
        #実行結果のハンドリング。
        if [ $?  -eq 9 ]; then    
          echo "処理が異常終了しました。"
          exit 9
        elif [ $? -eq 1 ]; then
          #q(quit)が選択された場合
          continue
        else
          echo " ${_menu_name} が完了しました。"
        fi
        ;;
      #シェルを終了
      "q") 
        echo "処理を終了します。"
        break
        ;;
      *) echo "[1-${_menu_num}]の範囲で指定してください。" 
        ;;
    esac

  done
}

####################################################################################################
#  メイン処理
####################################################################################################
#ファイルチェック用関数
function_inputfile_check
rtn=`echo $?`
if [ ${rtn} -ne 0 ];then
  exit 9
fi
function_menu
