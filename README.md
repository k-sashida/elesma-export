## 使用方法
#リリース
1. git cloneし"elesma-csvexport"のフォルダを選択し、zipファイル「elesma-csvexport.zipを作成する。

2.  「1.」で作成した資産を、サーバの任意のディレクトリにアップロード

3. 資産をアップロードしたディレクトリで以下のコマンドを実行します。
` unzip elesma-csvexport.zip`  

4. ディレクトリを移動します。
` cd elesma-csvexport`

5. 以下のコマンドを実行し権限付与を行います
`find ./ -type f -name "*.sh" -exec chmod +x {} \;`

6. "conf/csvexport.properties"を確認し、DBの接続情報を編集します。
（csvexport.properties一部抜粋)
#DB接続情報
##DB名
export readonly DBNAME=elesma
##スキーマ名
export readonly SCHEMENAME=elesma
##DBユーザ
export readonly DBUSER=elespdb
##DBユーザパスワード
export readonly DBUSERPASSWORD=elespdb
##DBホスト名
export readonly DBHOST=localhost
##DBポート
export readonly DBPORT=10130
（csvexport.properties一部抜粋おわり)

# 実行方法

1. 親シェルから起動する場合
  以下のコマンドを実行し画面遷移にしたがって実行する。
` ./csvexport.sh `

2. サブシェルを直接実行する場合（引数の詳細)
  ./sub/export_all_buildings.sh                  [-a|-e:必須]
  ./sub/export_all_informations.sh 
  ./sub/export_select_buildingid_buildings.sh    [建物番号:任意]
  ./sub/export_select_buildingid_informations.sh [建物番号:任意]
  ./sub/export_show_building.sh                  [建物番号:任意]
  ./sub/export_show_max_informationid.sh

## 画面イメージ
1.メイン画面(csvexport.sh 実行時

    【CSVエクスポートメニュー】
    
    [ No ]-[項目]
    [ 1 ] 全件エクスポート
    [ 2 ] 建物番号を指定してエクスポート
    [ 3 ] おしらせの全件エクスポート
    [ 4 ] 建物番号を指定しておしらせのエクスポート
    [ 5 ] おしらせID照会
    [ q ] シェルを終了
    
    項目を選択してください。Noで指定してください。[1-5 or q]

2.照会画面(./sub/export_show_building.sh 実行時)

[建物番号] - [ビル名] - [有効/無効] - [利用開始日] - [有効期限日]
9000001 -  ITCビル        - 有効 - 2019/01/01 - 2019/12/31

[エレベータ番号] - [エレベータ名]
002 - エレベータ２
[フロア名] - [ビーコンmajor] - [ビーコンminor] ##フロア情報については、ビーコンmajorの値が
   B1 -  2  -   2                              ## 設定されていない(nullの場合)はヘッダのみ
                                               ## 出力する。
[エレベータ番号] - [エレベータ名]
001 - エレベータ２
[フロア名] - [ビーコンmajor] - [ビーコンminor]
    2 -  2  -   1

## 注意事項
###  ・構成に記載したファイルを移動しないでください。
###  ・メニューを追加する場合は、menu.listに該当するメニューを追加し、追加したメニューに紐づくシェルを新規開発し、サブシェルの格納ディレクトリに、格納してください。
###  ・ m_elevators.csvの全件出力をした場合、件数が多すぎるとファイル取り込み処理に成功しない場合があります。
###     m_elevatorsをインポートする際には8000件程度になるようにしファイル取り込み処理を実行するようにしてください。
