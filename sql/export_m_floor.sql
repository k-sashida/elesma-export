SELECT
    f.building_id,
    f.elevator_number,
    f.floor,
    f.floor_name,
    f.beacon_uuid,
    f.beacon_major,
    f.beacon_minor,
    to_char(f.valid_date, 'yyyy/mm/dd'),
    to_char(f.invalid_date, 'yyyy/mm/dd')
FROM
    :SCHEME.m_floor f
    INNER JOIN :SCHEME.m_buildings b ON f.building_id = b.building_id
        
WHERE
--l.language_code = 'ja'

    (:BUILDINGID = ''
    AND (
            (:VALIDFLG = 'e'AND b.valid_flg = '1')
        OR  (:VALIDFLG = 'a')
        )
    )
OR  (:BUILDINGID <> ''
    AND f.building_id = :BUILDINGID
    )
    
ORDER BY f.building_id DESC,f.elevator_number DESC,f.floor DESC;
