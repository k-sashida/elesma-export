SELECT
   e.building_id,
   e.elevator_number,
   e.main_elevator_flg,
   l.elevator_name,
   l.gouki_name,
   e.main_floor,
   ST_Y(e.coordinate::geometry) as latitude,
   ST_X(e.coordinate::geometry) as longitude,
   e.indicator_turn_off_support_flg,
   '"' || e.indicator_turn_off_time_zone || '"' as indicator_turn_off_time_zone,
   e.invalid_time_zone,
   e.valid_flg,
   to_char(e.valid_date, 'yyyy/mm/dd'),
   to_char(e.invalid_date, 'yyyy/mm/dd'),
   e.slowly_closed_door_support_flg,
   e.info_id,
   e.car_beacon_uuid,
   e.car_beacon_major,
   e.car_beacon_minor
FROM
  :SCHEME.m_elevators_lng l
  inner JOIN :SCHEME.m_elevators e ON (e.building_id = l.building_id and e.elevator_number = l.elevator_number)
  inner JOIN :SCHEME.m_buildings b ON  e.building_id = b.building_id
WHERE
l.language_code = 'ja'
AND (
        (:BUILDINGID = ''
        AND (
                (:VALIDFLG = 'e'AND b.valid_flg = '1')
            OR  (:VALIDFLG = 'a')
            )
        )
    OR  (:BUILDINGID <> ''
        AND b.building_id = :BUILDINGID
        )
    )
ORDER BY
  e.building_id DESC, e.elevator_number DESC;
