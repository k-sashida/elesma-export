SELECT
    i.info_seq,
    i.building_id,
    i.elevator_number,
    i.force_display_flg,
    l.info,
    l.valid_flg,
    to_char(i.valid_date, 'yyyy/mm/dd'),
    to_char(i.invalid_date, 'yyyy/mm/dd')
FROM
    :SCHEME.m_info i
    JOIN :SCHEME.m_info_lng l ON  i.info_seq = l.info_seq
WHERE
    (
        :BUILDINGID = ''
    AND l.language_code = 'ja'
    )
OR  (
        :BUILDINGID <> ''
    AND (
            l.language_code = 'ja'
        AND (i.building_id = :BUILDINGID OR i.building_id is null)
        
        )
    )
ORDER BY
    i.info_seq DESC;
