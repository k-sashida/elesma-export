SELECT
    l.building_id,
    l.language_code,
    l.building_name,
    b.valid_flg
FROM
    :SCHEME.m_buildings_lng l
    JOIN :SCHEME.m_buildings b ON  b.building_id = l.building_id
WHERE
l.language_code <> 'ja'
AND (
        (:BUILDINGID = ''
        AND (
                (:VALIDFLG = 'e' AND b.valid_flg = '1')
            OR  (:VALIDFLG = 'a')
            )
        )
    OR  (:BUILDINGID <> ''
        AND b.building_id = :BUILDINGID
        )
    )
ORDER BY b.building_id DESC;
