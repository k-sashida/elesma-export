SELECT 
   e.building_id,
   e.elevator_number,
   l.language_code,
   l.elevator_name,
   l.gouki_name,
   e.valid_flg
FROM 
   :SCHEME.m_elevators_lng l
   inner JOIN :SCHEME.m_elevators e ON (e.building_id = l.building_id and e.elevator_number = l.elevator_number)
   inner JOIN :SCHEME.m_buildings b ON  (e.building_id = b.building_id)
WHERE 
l.language_code <> 'ja'
AND (
        (:BUILDINGID = ''
        AND (
                (:VALIDFLG = 'e' AND b.valid_flg = '1')
            OR  (:VALIDFLG = 'a')
            )
        )
    OR  (:BUILDINGID <> ''
        AND b.building_id = :BUILDINGID
        )
    )
ORDER BY 
  e.building_id DESC, e.elevator_number DESC;
