SELECT
  DISTINCT info_seq
FROM
  :SCHEME.m_info
ORDER BY
  info_seq DESC
  limit 1;
