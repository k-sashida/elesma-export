SELECT 
   l.info_seq,
   l.language_code,
   l.info,
   l.valid_flg
FROM 
  :SCHEME.m_info i
  JOIN :SCHEME.m_info_lng l ON i.info_seq = l.info_seq 
WHERE 
    (
        :BUILDINGID = ''
    AND l.language_code <> 'ja'
    )
OR  (
        :BUILDINGID <> ''
    AND (
            l.language_code <> 'ja'
        AND (i.building_id = :BUILDINGID OR i.building_id is null )
        )
    )
ORDER BY 
   i.info_seq DESC;

