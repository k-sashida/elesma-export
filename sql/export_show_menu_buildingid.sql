SELECT
  b.building_id,
  bl.building_name,
  CASE
        when b.valid_flg = 1 THEN '有効'
        when b.valid_flg = 0 THEN '無効'
        ELSE NULL
  END AS "valid_flg",  
  to_char(b.valid_date, 'yyyy/mm/dd'),
  to_char(b.invalid_date, 'yyyy/mm/dd'),
  trim (both FROM e.elevator_number),
  el.elevator_name,
  e.car_beacon_major,
  e.car_beacon_minor,
  f.floor_name,
  f.beacon_major,
  f.beacon_minor
FROM 
  :SCHEME.m_floor f
  JOIN :SCHEME.m_elevators e ON (f.building_id = e.building_id AND f.elevator_number = e.elevator_number)
  JOIN :SCHEME.m_elevators_lng el ON (e.building_id = el.building_id AND e.elevator_number = el.elevator_number) 
  JOIN :SCHEME.m_buildings b ON f.building_id = b.building_id
  JOIN :SCHEME.m_buildings_lng bl ON f.building_id = bl.building_id
WHERE 
  f.building_id = :BUILDINGID
  AND
  bl.language_code = 'ja'
  AND
  el.language_code = 'ja'
ORDER BY f.elevator_number DESC,f.floor DESC;
